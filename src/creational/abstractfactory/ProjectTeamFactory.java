package creational.abstractfactory;

import creational.abstractfactory.Developer;

public interface ProjectTeamFactory {
    Developer getDeveloper();
    Tester getTester();
    ProjectManager getProjectManager();
}
