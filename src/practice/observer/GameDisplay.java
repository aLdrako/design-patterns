package practice.observer;

public class GameDisplay implements Observer {

    private int health;
    private int armor;
    private int stamina;
    private final Character character;

    public GameDisplay(Character character) {
        this.character = character;
        character.registerObserver(this);
    }

    @Override
    public void update() {
        this.health = character.getHealth();
        this.armor = character.getArmor();
        this.stamina = character.getStamina();
        display();
    }

    private void display() {
        System.out.printf("Character's health: %d, armor: %d, stamina: %d%n", health, armor, stamina);
    }
}
