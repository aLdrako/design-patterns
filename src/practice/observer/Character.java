package practice.observer;

import java.util.ArrayList;
import java.util.List;

public class Character implements Subject {

    private final List<Observer> observers;
    private int health = 100;
    private int armor = 100;
    private int stamina = 100;

    public Character() {
        observers = new ArrayList<>();
    }

    @Override
    public void registerObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObserver() {
        for (Observer observer : observers) {
            observer.update();
        }
    }

    private void statsChanged() {
        notifyObserver();
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
        statsChanged();
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
        statsChanged();
    }

    public int getStamina() {
        return stamina;
    }

    public void setStamina(int stamina) {
        this.stamina = stamina;
        statsChanged();
    }
}
