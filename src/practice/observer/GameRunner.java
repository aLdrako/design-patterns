package practice.observer;

public class GameRunner {
    public static void main(String[] args) {
        Character character = new Character();
        new GameDisplay(character);

        character.setHealth(95);
        character.setArmor(80);
    }
}
