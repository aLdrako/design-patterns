package practice.strategy;

public abstract class Character {
    WeaponBehavior weaponBehavior;
    SkillBehavior skillBehavior;

    public void performAttack() {
        weaponBehavior.useWeapon();
    }

    public void performSkill() {
        skillBehavior.useSkill();
    }

    public abstract void attack();

    public void setWeaponBehavior(WeaponBehavior weaponBehavior) {
        this.weaponBehavior = weaponBehavior;
        System.out.println("Changing Weapon to: " + weaponBehavior.toString());
    }

    public void setSkillBehavior(SkillBehavior skillBehavior) {
        this.skillBehavior = skillBehavior;
        System.out.println("Changing Skill to: " + skillBehavior.toString());
    }
}
