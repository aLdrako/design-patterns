package practice.strategy;

public class Sword implements WeaponBehavior {

    @Override
    public void useWeapon() {
        System.out.println("Attack with Sword");
    }

    @Override
    public String toString() {
        return "Sword";
    }
}
