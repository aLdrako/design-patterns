package practice.strategy;

public class GameRunner {
    public static void main(String[] args) {
        Character doomGuy = new DoomGuy();
        doomGuy.attack();
        doomGuy.performAttack();

        doomGuy.setWeaponBehavior(new Sword());
        doomGuy.performAttack();

        System.out.println("=".repeat(30));
        Character demon = new Demon();
        demon.attack();
        demon.performAttack();
        demon.performSkill();
        demon.setSkillBehavior(new Teleport());
        demon.performSkill();
    }
}
