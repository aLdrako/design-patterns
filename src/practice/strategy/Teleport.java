package practice.strategy;

public class Teleport implements SkillBehavior {

    @Override
    public void useSkill() {
        System.out.println("Teleporting...");
    }

    @Override
    public String toString() {
        return "Teleport";
    }
}
