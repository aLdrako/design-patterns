package practice.strategy;

public class Fireball implements WeaponBehavior {

    @Override
    public void useWeapon() {
        System.out.println("Attack with Fireball");
    }

    @Override
    public String toString() {
        return "Fireball";
    }
}
