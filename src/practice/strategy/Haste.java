package practice.strategy;

public class Haste implements SkillBehavior {

    @Override
    public void useSkill() {
        System.out.println("Increase Haste");
    }

    @Override
    public String toString() {
        return "Haste";
    }
}