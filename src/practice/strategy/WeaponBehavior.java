package practice.strategy;

public interface WeaponBehavior {
    void useWeapon();
}
