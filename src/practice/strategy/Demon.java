package practice.strategy;

public class Demon extends Character {

    public Demon() {
        weaponBehavior = new Fireball();
        skillBehavior = new Haste();
    }

    @Override
    public void attack() {
        System.out.println("Demon Attacking");
    }
}
