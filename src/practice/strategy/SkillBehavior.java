package practice.strategy;

public interface SkillBehavior {

    void useSkill();
}
