package practice.strategy;

public class DoomGuy extends Character {

    public DoomGuy() {
        weaponBehavior = new Shotgun();
    }

    @Override
    public void attack() {
        System.out.println("Doom Guy Attacking");
    }

}
