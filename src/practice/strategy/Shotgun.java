package practice.strategy;

public class Shotgun implements WeaponBehavior {

    @Override
    public void useWeapon() {
        System.out.println("Attack with Shotgun");
    }

    @Override
    public String toString() {
        return "Shotgun";
    }
}
